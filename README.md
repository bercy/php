Example PHP project
===================

The purpose of this repository is to show how to use Travis CI to do
continuous integration with a PHP project.

Here is a sample status icon showing the state of the master branch.

[![Build Status](https://travis-ci.org/bercy/php.svg?branch=master)](https://travis-ci.org/bercy/php)
[![Codacy Badge](https://api.codacy.com/project/badge/grade/201176b98d034bc6a905eed4da890566)](https://www.codacy.com/app/balazs-micskey/php)
[![Code Climate](https://codeclimate.com/github/bercy/php/badges/gpa.svg)](https://codeclimate.com/github/bercy/php)
[![Issue Count](https://codeclimate.com/github/bercy/php/badges/issue_count.svg)](https://codeclimate.com/github/bercy/php)

In order to run this project just fork it on github.com and then [enable](http://about.travis-ci.org/docs/user/getting-started/)
your fork on your [travis-ci profile](http://travis-ci.org/profile). Every push will then trigger a new build on Travis CI.

(Don't forget to update the badge url in the README to point to your own travis project.)
